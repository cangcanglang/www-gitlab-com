---
layout: markdown_page
title: "Product Manager Responsibilities and Tasks"
---

The product manager reports to the CEO and is an individual contributor.

## Development direction

- Manage CE / EE / CI features: make sure a release contains attractive new features.
- Manage customer requests: take load of support for handling this and making sure we deliver
- Create direction in development efforts: Making sure that our long term goals are closer with each release
- Make sure other products (license, version) are not snowed under with the release
- Handle feature requests from sales
- The CTO has a final say on all features, the CEO will also be involved in many decisions.

## Public information

- Guard the quality of any public materials, such as documentation and tweets
- keep the feature request tracker up to date
- Make sure developers write good docs for every function
- Make sure the CE/EE comparison, GitHub comparison and battlecards
are up to date
- Ensure that answers to internal questions end up being documented publicly
- Move most of the issues from dev to .com (so our internal roadmap is public)
- Make [/direction](/direction) a high level roadmap (can link to issues on .com)

## Customer relations

- Join customer visits if they can lead to new features
- Join partner visits if they can lead to new features
- Make sure the demo scripts are up to date and train people
- Move feature forum to GitLab issues (once [award emoji](https://dev.gitlab.org/gitlab/gitlabhq/issues/2388) is out)

## Marketing

- Make sure the release announcements are well written and cover everything
- Do regular feature highlights
- Ensure sales has proper marketing materials for our features
- Answer feature questions on social media (twitter, stack overflow, mailinglist)
- Make sure the website is effective (good content, well presented)
- Make sure the website infrastructure is effective, including being the lead for [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com/)